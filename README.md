Microchip MCP9804 class for Arduino
========

You can use this class to allow your Arduino based system to communicate with a Micorchip MCP9804 temperature sensor through I2C.

##Main features
* Easy configuration
* High level method calls for I2C commands
